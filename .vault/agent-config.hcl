pid_file = "./pidfile"

auto_auth {
   method "approle" {
       mount_path = "auth/approle"
       config = {
           role_id_file_path = "/home/ubuntu/.role_id"
           secret_id_file_path = "/home/ubuntu/.secret_id"
           remove_secret_id_file_after_reading = false
       }
   }

   sink "file" {
       config = {
           path = ".sink"
       }
   }
}
exit_after_auth=true

template {
  source      = "./.vault/templates/rendered-db.variables.tmpl"
  destination = "./database/.db.variables.env"
}
