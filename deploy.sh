#!/bin/bash -e

ENV=$ENV PROJECT=$PROJECT APPLICATION=$APPLICATION envsubst < .vault/templates/db.variables.tmpl > .vault/templates/rendered-db.variables.tmpl
# Retrieve secrets
vault agent -log-level=WARN -config=.vault/agent-config.hcl

dcp_up (){
    cd $1
    docker-compose up -d
    cd ..
}

dcp_up "./traefik"
dcp_up "./database"